def function_organize_img(data_path, percentage_train_in_percent):
    """
    This function allows you to organize images and masks which are corresponding to the images in differents folders. All the images are in the 3 first folders and the masks in the fourth folder. To use the function, you have to input the path of the folder which contains the four folders of the images and the masks, and the percentage in percent of the images which be used for the training. Then the code return "Images and masks have been organized" if the organization of the files didn't have already been and "The images and masks have already been organized before" if it already have been.
    """
    import os
    import shutil
    
    #creation of a new folder which contains the data organized if it doesn't exist
    if (os.path.isdir("../data_organized"))==False: # Check if the folder "data_organized" exists or not
        path_folder_data_organized = "../data_organized"
        os.mkdir(path_folder_data_organized)
    
    #Then, we need to create folders for the training and testing, with inside folders "images" and  "masks"
    path_test = "../data_organized/test"
    path_train = "../data_organized/train"
    
    if (os.path.isdir(path_test))==False: # Check if the folder "test" exists or not
        os.mkdir(path_test) # creation of a folder "test"
    if (os.path.isdir(path_train))==False: # Check if the folder "train" exists or not
        os.mkdir(path_train) # creation of a folder "train"
    
    path_test_image = "../data_organized/test/image"
    path_test_mask = "../data_organized/test/mask"
    path_train_image = "../data_organized/train/image"
    path_train_mask = "../data_organized/train/mask"
    
    if (os.path.isdir(path_test_image))==False: # Check if the folder "test/image" exists or not
        os.mkdir(path_test_image) # creation of a folder "image" inside "test"
    if (os.path.isdir(path_test_mask))==False: # Check if the folder "test/mask" exists or not
        os.mkdir(path_test_mask) # creation of a folder "mask" inside "test"
    if (os.path.isdir(path_train_image))==False: # Check if the folder "train/image" exists or not
        os.mkdir(path_train_image) # creation of a folder "image" inside "train"
    if (os.path.isdir(path_train_mask))==False: # Check if the folder "train/mask" exists or not
        os.mkdir(path_train_mask) # creation of a folder "mask" inside "train"
    
    fold_mask = os.listdir(data_path)[3] #Name of the folder of the masks
    path_folder_masks = os.path.join(data_path,fold_mask) #Path where are the masks
    
    masks = os.listdir(path_folder_masks)
    
    
    all_folders = os.listdir(data_path) # All the folders
    folders = all_folders[0:3] # Only the folders with images and not the mask
    
    #Distribution of the images and the masks in the organized folders
    
    size_mask = len(masks)-1 # -1 because we don't care about the last file of masks
    
    pourcent_train_decimal = percentage_train_in_percent/100
    
    number_image_for_train = pourcent_train_decimal*size_mask
    
    round_number_train = round(number_image_for_train)
    
    if len(os.listdir(path_train_mask)) == 0 :
    
        counter = 0

        for each_mask in masks[0:size_mask]:   # We are looking all the files of masks file by file
            for fold in folders:   # And for each file of mask we are looking if a file in folders is corresponding with the mask
                path_we_use = data_path+"/"+fold  #The path of the fold we are studying
                files = os.listdir(path_we_use)  # The list of the files inside the folder we are studying
                for f in files:
                    full_name = fold + "-" + f 
                    if each_mask == full_name.replace(".TIF","_cellpose_curated.tif"):  # We are looking if the image is corresponding with the mask with the good name
                        if counter <= (round_number_train-1):  # In this case we are put the image and the mask in the folder for training
                            path_mask = path_train_mask
                            path_img = path_train_image
                        else:                                 # In this case we are put the image and the mask in the folder for testing
                            path_mask = path_test_mask
                            path_img = path_test_image


                        shutil.copy2(os.path.join(path_we_use,f), os.path.join(path_img,full_name.replace(".TIF","_cellpose_curated.tif"))) # We put the images in the folder for images
                        shutil.copy2(os.path.join(path_folder_masks,each_mask), path_mask) # We put the masks in the folder for masks
                        counter+=1

        print("Images and masks have been organized")
        
    else:
        print("The images and masks have already been organized before")
    
    return





def divide_image_short(image_path,new_number_lin,new_number_col):
    """
    This function divides an image into several images with a certain size we want. You enter the image path, the new_number_lin which is the height of the new images (or the number of lines), and the new_number_col which is the width of the new images (or the number of columns). If the size of the initial image doesn't fit with th size of the new images, which means that the width and the height of the new images are not perfect divisors of the width and the height of the initial image, it still works but you will have images with smaller sizes in addition to the images with sizes we want.
    You also can remove the images with no informations in them, like black images, by erasing them or puting them into a new folder, if you want.
    """
    
    import os
    import numpy as np
    import matplotlib.pyplot as plt
    import skimage.io as io
    import PIL
    from PIL import Image
    import shutil

    
    path_folder_cropped_images = "../cropped_images"  #create the new folder
    name_folder_image = os.path.basename(image_path)
    
    # Creation of a new folder which will contain the new images
    if (os.path.isdir(path_folder_cropped_images))==False: # Check if the folder "cropped_images" exists or not
        os.mkdir(path_folder_cropped_images)
        
    # Creation of a sub-folder consecrated to the image
    if (os.path.isdir(os.path.join(path_folder_cropped_images,name_folder_image)))==False: # Check if the folder "cropped_images" exists or not
        os.mkdir(path_folder_cropped_images+"/"+name_folder_image)
        
    new_folder_for_saving = path_folder_cropped_images+"/"+name_folder_image
        
    image = io.imread(image_path) # Read the image
    img_pil = Image.open(image_path) #Open the image
    format_im = img_pil.format  # Get the format of the image, for exe : tif, png, ...
    height = img_pil.height  # get the number of lines
    width = img_pil.width  # get the numbers of columns
    
    # In case when the new number of lines or the new number of columns doesn't fit very well with the current number of lines and columns, we have to check the size of the last image of the column or line
    flg_col = 0
    flg_lin = 0
    
    if (width%new_number_col)!=0:    # In this case, it doesn't fit with the columns
        flg_col = 1
        
    if (height%new_number_lin)!=0:    # In this case, it doesn't fit with the lines
        flg_lin = 1
    
    div_width = width//new_number_col + flg_col  # number of images on each line
    div_height = height//new_number_lin + flg_lin  # number of images on each column
    
    
    for i in range(0,div_width):
        for j in range(0,div_height):
            (left, upper, right, lower) = (i*new_number_col, j*new_number_lin, i*new_number_col+new_number_col, j*new_number_lin+new_number_lin)
            new_im = img_pil.crop((left, upper, right, lower)) # 'Crop' allow the selections of a part of an image, from points which delimit the part
            new_im.save(new_folder_for_saving+"/new_im"+"_"+str(j)+"_"+str(i)+"."+format_im)
            new_im.close()  #We have to close the image after its utilization because we can meet problems after
            
    if flg_col == 0 and flg_lin == 1: #In the case where it doesn't fit well for the height, aka the number of lines
        for k in range(0,div_width):
            (left, upper, right, lower) = (k*new_number_col, (height//new_number_lin)*new_number_lin, k*new_number_col+new_number_col, (height//new_number_lin)*new_number_lin+height%new_number_lin)
            new_im = img_pil.crop((left, upper, right, lower))
            new_im.save(new_folder_for_saving+"/new_im"+"_"+str(div_height-1)+"_"+str(k)+"."+format_im)
            new_im.close()
    if flg_col == 1 and flg_lin == 0: #In the case where it doesn't fit well for the width, aka the number of columns
        for k in range(0,div_height):
            (left, upper, right, lower) = ((width//new_number_col)*new_number_col, k*new_number_lin, (width//new_number_col)*new_number_col+width%new_number_col, k*new_number_lin+new_number_lin)
            new_im = img_pil.crop((left, upper, right, lower))
            new_im.save(new_folder_for_saving+"/new_im"+"_"+str(k)+"_"+str(div_width-1)+"."+format_im)
            new_im.close()
    if flg_col == 1 and flg_lin == 1: #In the case where it doesn't fit well for the width and the height 
        # For the last column of the initial image
        for k in range(0,div_height-1):
            (left, upper, right, lower) = ((width//new_number_col)*new_number_col, k*new_number_lin, (width//new_number_col)*new_number_col+width%new_number_col, k*new_number_lin+new_number_lin)
            new_im = img_pil.crop((left, upper, right, lower))
            new_im.save(new_folder_for_saving+"/new_im"+"_"+str(k)+"_"+str(div_width-1)+"."+format_im)
            new_im.close()
        # For the last line of the initial image
        for l in range(0,div_width-1):
            (left, upper, right, lower) = (k*new_number_col, (height//new_number_lin)*new_number_lin, k*new_number_col+new_number_col, (height//new_number_lin)*new_number_lin+height%new_number_lin)
            new_im = img_pil.crop((left, upper, right, lower))
            new_im.save(new_folder_for_saving+"/new_im"+"_"+str(div_height-1)+"_"+str(l)+"."+format_im)
            new_im.close()
        # For the very last image (of the last column and the last line of the initial image)
        (left_f, upper_f, right_f, lower_f) = ((width//new_number_col)*new_number_col, (height//new_number_lin)*new_number_lin, (width//new_number_col)*new_number_col+width%new_number_col, (height//new_number_lin)*new_number_lin+height%new_number_lin)
        new_im = img_pil.crop((left_f, upper_f, right_f, lower_f))
        new_im.save(new_folder_for_saving+"/new_im"+"_"+str(div_height-1)+"_"+str(div_width-1)+"."+format_im)
        new_im.close()
        
                  
    ## Now, we want to throw away the images which contain nothing, which are just black##
    
    
    #First we ask the user if he/she wants to remove the black images
    rem_black_images = input("Do you want to remove the black images from the folder ? y or n : ")
    
    if rem_black_images=="y":
        new_fold_or_not = input("Do you want to put them in a new folder (folder) or erase them (erase) ? folder or erase : ")
        
        flag = 0
        
        if new_fold_or_not == "folder":
            path_folder_black_images = new_folder_for_saving+"/black_images"
            if (os.path.isdir(path_folder_black_images))==False:
                os.mkdir(path_folder_black_images) #create the new folder for black images
                
                flag = 1
            
                
        images = os.listdir(new_folder_for_saving) # We get the list all the images saved before
        nb_images = len(images)
    
        nb = 0
        for i in range(flag, nb_images):
            path_new_im_f = new_folder_for_saving+"/"+images[i]
            new_im_f = Image.open(path_new_im_f)
            if (new_im_f.getbbox())==None: # We check if the image is black
                nb+=1
                new_im_f.close()
                if new_fold_or_not == "erase":
                    os.remove(path_new_im_f) # If it's black, we remove the image from the folder
                if new_fold_or_not == "folder":
                    shutil.copy2(path_new_im_f, path_folder_black_images+"/black_img_"+str(nb)+"."+format_im)
                    os.remove(path_new_im_f)
                    
    else:
        return
            
    return




def create_crops(img_path, nZ, nY, nX):  #The Tuple (nZ, nY, nX) doesn't work as an input for a function
    """
    This function creates crops from an initial image. You can choose the size of the new images, but the new size have to be in match withe the size of the initial image. The inputs of this function are : the image (the path of the image), the length on the axis z (nZ), the length on the axis y (nY), and the length on the axis x. It will created a new folder with the name of the initial image inside the folder "cropped_images". We can also choose or not to delete the images with no informations on it (black images).
    This function uses Numpy.
    """
    
    # We admit that the chosen size of the new images are in a perfect match with the size of the initial image
    # Moreover, if the image is in 2D, the user has to put nZ=0
    
    import skimage.io as io
    import numpy as np
    import os
    import matplotlib.pyplot as plt
    
    import PIL  # Just to find the format
    from PIL import Image
    
    
    
    path_folder_cropped_images = "../cropped_images"  #create the new folder
    image = io.imread(img_path) # Read the image
    name_folder_new_img = os.path.basename(img_path)
    path_folder_new_img = path_folder_cropped_images+"/"+name_folder_new_img
    
    
    img_pil = Image.open(img_path) #Open the image
    format_im = img_pil.format  # Get the format of the image, for exe : tif, png, ...
    
    size = image.shape # ZYXTC
    init_z = size[0]
    init_y = size[1]
    init_x = size[2]
    
    # Just to check if the new size fit well with the size of the initial image
    drap = 0
    if (init_z%nZ)!=0:
        drap = 1
    if (init_y%nY)!=0:
        drap = 1
    if (init_x%nX)!=0:
        drap = 1
    if drap==1:
        print("The size of new images doesn't fit with the size of the initial image")
        return
        
    
    # Creation of a new folder which will contain the new images
    if (os.path.isdir(path_folder_cropped_images))==False: # Check if the folder "cropped_images" exists or not
        os.mkdir(path_folder_cropped_images)
    
    if (os.path.isdir(path_folder_new_img))==False:  # Check if the folder for the specific image exists or not
        os.mkdir(path_folder_new_img)
        
    
    
    div_lin_y = init_y//nY  # number of elements on the axis y
    div_col_x = init_x//nX  # number of elements on the axis x
    if nZ != 0:
        div_thick_z = init_z//nZ  # number of elements on the axis z
        
    flag_x = 0
    flag_y = 0
    flag_z = 0
    

            
    if nZ != 0:
        for z in range(div_thick_z):
            flag_y = 0
            for y in range(div_lin_y):
                flag_x = 0
                for x in range(div_col_x):
                    crop = image[flag_z:flag_z+nZ, flag_y:flag_y+nY, flag_x:flag_x+nX]
                    taille_im = crop.shape
                    # print(taille_im)
                    # taille_une_couche = crop[1,:,:].shape
                    # plt.imshow(crop[1,:,:])
                    name_img = path_folder_new_img+"/"+"new_img_"+"z="+str(z)+"_"+"y="+str(y)+"_"+"x="+str(x)+"."+format_im
                    io.imsave(name_img, crop)
                    flag_x += nX
                flag_y += nY
            flag_z += nZ
            
    if nZ == 0:
        for y in range(div_lin_y):
                flag_x = 0
                for x in range(div_col_x):
                    crop = image[flag_z:flag_z+nZ, flag_y:flag_y+nY, flag_x:flag_x+nX]
                    taille_im = crop.shape
                    # print(taille_im)
                    # taille_une_couche = crop[1,:,:].shape
                    # plt.imshow(crop[1,:,:])
                    name_img = path_folder_new_img+"/"+"new_img_"+"z="+str(z)+"_"+"y="+str(y)+"_"+"x="+str(x)+"."+format_im
                    io.imsave(name_img, crop)
                    flag_x += nX
                flag_y += nY
            
    ## Now, we want to throw away the images which contain nothing, which are just black##
    
    
    #First we ask the user if he/she wants to remove the black images
    rem_black_images = input("Do you want to remove the black images from the folder if there is any  ? y or n : ")
    
    if rem_black_images=="y":
        new_fold_or_not = input("Do you want to put them in a new folder (folder) or delete them (delete) ? folder or delete : ")
        
        flag = 0
        
        if new_fold_or_not == "folder":
            path_folder_black_images = path_folder_new_img+"/black_images"
            if (os.path.isdir(path_folder_black_images))==False:
                os.mkdir(path_folder_black_images) #create the new folder for black images
                
                flag = 1
            
                
        images = os.listdir(path_folder_new_img) # We get the list all the images saved before
        nb_images = len(images)
    
        message_black_img = 0
        message_black_img_folder = 0
        message_black_img_delete = 0
    
        nb = 0
        for i in range(flag, nb_images):
            path_new_im_f = path_folder_new_img+"/"+images[i]
            new_im_f = Image.open(path_new_im_f)
            if (new_im_f.getbbox())==None: # We check if the image is black
                nb+=1
                message_black_img = 1
                new_im_f.close()
                if new_fold_or_not == "delete":
                    os.remove(path_new_im_f) # If it's black, we remove the image from the folder
                    message_black_img_delete = 1
                if new_fold_or_not == "folder":
                    shutil.copy2(path_new_im_f, path_folder_black_images+"/black_img_"+str(nb)+"."+format_im)
                    os.remove(path_new_im_f)
                    message_black_img_folder = 1
                    
    if message_black_img==0:
        print("There is no black images")
    if message_black_img_delete==1:
        print("Black images have been deleted")
    if message_black_img_folder==1:
        pront("Black images have been saved in another folder")
                    
    else:
        return
                
           