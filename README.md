# Images processing utilities

## Author
Constance Forestier

## Functions description
**function_organize_img :** 
This function allows you to organize images and masks which are corresponding to the images in differents folders. All the images are in the 3 first folders and the masks in the fourth folder. To use the function, you have to input the path of the folder which contains the four folders of the images and the masks, and the percentage in percent of the images which be used for the training. Then the code return "Images and masks have been organized" if the organization of the files didn't have already been and "The images and masks have already been organized before" if it already have been.


**divide_image_short :**
This function divides an image into several images with a certain size we want. You enter the image path, the new_number_lin which is the height of the new images (or the number of lines), and the new_number_col which is the width of the new images (or the number of columns). If the size of the initial image doesn't fit with th size of the new images, which means that the width and the height of the new images are not perfect divisors of the width and the height of the initial image, it still works but you will have images with smaller sizes in addition to the images with sizes we want.
You also can remove the images with no informations in them, like black images, by erasing them or puting them into a new folder, if you want.


**def create_crops :**
This function creates crops from an initial image. You can choose the size of the new images, but the new size have to be in match withe the size of the initial image. The inputs of this function are : the image (the path of the image), the length on the axis z (nZ), the length on the axis y (nY), and the length on the axis x. It will created a new folder with the name of the initial image inside the folder "cropped_images". We can also choose or not to delete the images with no informations on it (black images).
This function uses Numpy, excepts for the removing of the black images.
